const btns = document.querySelectorAll(`.btn`);
document.addEventListener("keydown", (event) => {
    btns.forEach((elem) => {
        elem.style.background = `black`;
        const data = elem.dataset.f;
        if (event.code == data) {
            elem.style.background = `blue`;
        }
    })
})
